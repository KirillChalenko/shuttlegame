﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Player controller and behavior
/// </summary>
public class PlayerScript : MonoBehaviour {

	public Vector2 speed = new Vector2(50.0f, 50.0f);
	private Vector2 _movement;
	private float _speed = 2.0f;
	private Vector3 _touchPosition;
	private int _collisionDamage = 1;
	private WeaponScript _weapon;

	void Awake() {
		_weapon = GetComponent<WeaponScript>();
	}

	void Start() {
		_touchPosition = transform.position;
	}

	// Update is called once per frame
	void Update () {

#if (UNITY_IPHONE || UNITY_ANDROID)
		// check if user touch the screen, then store touch position
		if (Input.touchCount > 0) {
			Touch firstTouch = Input.touches[0];
			if (firstTouch.phase == TouchPhase.Ended) {
				_touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(firstTouch.position.x, firstTouch.position.y, 0.0f));
				_touchPosition.z = 0.0f;
			}
		}
		
		// move smoothly the player to the touch position
		transform.position = Vector3.Lerp(transform.position, _touchPosition, _speed * Time.deltaTime);

		// auto-fire
		if (_weapon != null && _weapon.CanAttack) {
			_weapon.Attack(false);
			//SoundEffectsHelper.Instance.MakePlayerShotSound();
		}
#else
		// calculate movement
		float inputX = Input.GetAxis("Horizontal");
		float inputY = Input.GetAxis("Vertical");
		
		_movement = new Vector2(speed.x * inputX, speed.y * inputY);

		// check if user makes fire
		if (Input.GetButtonDown("Fire1") && _weapon != null && _weapon.CanAttack) {
			_weapon.Attack(false);
			//SoundEffectsHelper.Instance.MakePlayerShotSound();
		}
#endif
		// keep player into the camera bounds
		float dist = (transform.position - Camera.main.transform.position).z;

		float leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		float rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		float topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
		float bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;

		transform.position = new Vector3(
			Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
			Mathf.Clamp(transform.position.y, topBorder, bottomBorder),
			transform.position.z
			);
	}

	void FixedUpdate() {
#if !(UNITY_IPHONE || UNITY_ANDROID)
		rigidbody2D.velocity = _movement;
#endif
	}

	void OnCollisionEnter2D(Collision2D collision) {
		bool damagePlayer = false;

		// check if we have collision with an enemy
		EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();
		if (enemy != null) {
			// kill enemy
			HealthScript enemyHealth = enemy.gameObject.GetComponent<HealthScript>();
			if (enemyHealth != null) {
				enemyHealth.Kill();
			}

			damagePlayer = true;
		}

		if (damagePlayer) {
			HealthScript playerHealth = GetComponent<HealthScript>();
			if (playerHealth != null) {
				playerHealth.TakeDamage(_collisionDamage);
			}
		}
	}

	void OnDestroy() {
		transform.parent.gameObject.AddComponent<GameOverScript>();
	}
}
