﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Parallax scrolling script that should be assigned to a layer
/// </summary>
public class ScrollingScript : MonoBehaviour {

	public Vector2 speed = new Vector2(2.0f, 2.0f);
	public Vector2 direction = new Vector2(-1.0f, 0.0f);
	// Movement should be applied to camera
	public bool isLinkedToCamera = false;
	public bool isLooping = false;
	// List of children with a renderer
	private List<Transform> _backgroundPart;

	void Start () {
		if (isLooping) {
			_backgroundPart = new List<Transform>();

			// get all visible children of the layer
			for (int i = 0; i < transform.childCount; i++) {
				Transform child = transform.GetChild(i);
				if (child.renderer != null) {
					_backgroundPart.Add(child);
				}
			}

			// sort by position
			_backgroundPart = _backgroundPart.OrderBy(t => t.position.x).ToList();
		}
	}
	
	// Update is called once per frame
	void Update () {
		// move object
		Vector3 movement = new Vector3(speed.x * direction.x, speed.y * direction.y, 0.0f);
		movement *= Time.deltaTime;

		transform.Translate(movement);

		// move camera
		if (isLinkedToCamera) {
			Camera.main.transform.Translate(movement);
		}

		// make loop if needed
		if (isLooping) {
			// get the first object
			Transform firstChild = _backgroundPart.FirstOrDefault();

			if (firstChild != null) {
				// Check if the child is already (partly) before the camera.
				if (firstChild.position.x < Camera.main.transform.position.x) {
					// If the child is already on the left of the camera,
					// we test if it's completely outside and needs to be recycled.
					if (firstChild.renderer.IsVisibleFrom(Camera.main) == false) {
						// Get the last child position.
						Transform lastChild = _backgroundPart.LastOrDefault();
						Vector3 lastPosition = lastChild.transform.position;
						Vector3 lastSize = (lastChild.renderer.bounds.max - lastChild.renderer.bounds.min);
						
						// Set the position of the recyled one to be AFTER the last child.
						firstChild.position = new Vector3(lastPosition.x + lastSize.x, firstChild.position.y, firstChild.position.z);

						_backgroundPart.Remove(firstChild);
						_backgroundPart.Add(firstChild);
					}
				}
			}
		}
	}
}
