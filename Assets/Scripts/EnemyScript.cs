﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy generic behavior
/// </summary>
public class EnemyScript : MonoBehaviour {

	protected WeaponScript _weapon;
	protected MoveScript _move;
	protected bool _hasSpawn;

	protected virtual void Awake() {
		_weapon = GetComponentInChildren<WeaponScript>();
		_move = GetComponent<MoveScript>();
	}

	protected virtual void Start() {
		_hasSpawn = false;

		// disable everything
		collider2D.enabled = false;
		_move.enabled = false;
		_weapon.enabled = false;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if (!_hasSpawn) {
			// spawn the enemy if we inside the camera
			if (renderer.IsVisibleFrom(Camera.main)) {
				Spawn();
			}
		}
		else {
			// auto-fire
			if (_weapon != null && _weapon.enabled && _weapon.CanAttack) {
				_weapon.Attack(true);
				SoundEffectsHelper.Instance.MakeEnemyShotSound();
			}

			// destroy the enemy if we outside the camera
			if (!renderer.IsVisibleFrom(Camera.main)) {
				Destroy(gameObject);
			}
		}
	}

	protected virtual void Spawn() {
		_hasSpawn = true;

		// enable everything
		collider2D.enabled = true;
		_move.enabled = true;
		_weapon.enabled = true;
	}
}
