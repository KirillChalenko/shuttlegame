﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handle hitpoints and damages
/// </summary>
public class HealthScript : MonoBehaviour {

	// total hitpoints
	public int hp = 1;

	// enemy or player?
	public bool isEnemy = true;
	
	// Inflicts damage and check if the object should be destroyed
	public void TakeDamage(int damageCount) {
		hp -= damageCount;

		if (!isEnemy) {
			GUIHelper.Instance.UpdateUserHP(hp);
		}

		if (hp <= 0) {
			// we are dead
			SpecialEffectsHelper.Instance.Explosion(transform.position);
			SoundEffectsHelper.Instance.MakeExplosionSound();
			if (isEnemy) {
				GUIHelper.Instance.EnemyKilled();
			}
			Destroy(gameObject);
		}
	}

	// kill a game object
	public void Kill() {
		TakeDamage(hp);
	}

	void Start() {
		if (!isEnemy) {
			GUIHelper.Instance.UpdateUserHP(hp);
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider) {
		ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();

		// check if we deal with a shot object
		if (shot != null) {
			// avoid friendly fire
			if (shot.isEnemyShot != isEnemy) {
				TakeDamage(shot.damage);
				Destroy(shot.gameObject);
			}
		}
	}
}
