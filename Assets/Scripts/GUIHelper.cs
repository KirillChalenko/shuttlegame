﻿using UnityEngine;
using System.Collections;

public class GUIHelper : MonoBehaviour {

	public static GUIHelper Instance;

	private float _userPoints = 0.0f;
	private int _userHP = 0;
	private GUISkin _skin;

	private const float HORIZONTAL_MARGIN = 20.0f;
	private const float VERTICAL_MARGIN = 20.0f;
	private const float LABEL_WIDTH = 100.0f;
	private const float LABEL_HEIGHT = 50.0f;

	private const float POINTS_PER_ENEMY = 10.0f;
	private const float POINTS_PER_BOSS = 50.0f;

	void Awake() {
		if (Instance != null) {
			Debug.LogError("Multiple instances of GUIHelper!");
		}
		
		Instance = this;
	}

	void Start() {
		_skin = Resources.Load("ShuttleGameGUISkin") as GUISkin;
	}

	void OnGUI() {
		GUI.skin = _skin;

		// show user points
		Rect frame = new Rect (HORIZONTAL_MARGIN, VERTICAL_MARGIN, LABEL_WIDTH, LABEL_HEIGHT);
		GUI.Label(frame, string.Format("Points: {0}", _userPoints));

		// show user hp
		frame.x = Screen.width - LABEL_WIDTH - HORIZONTAL_MARGIN;
		GUI.Label(frame, string.Format("HP: {0}", _userHP));
	}

	public void EnemyKilled() {
		_userPoints += POINTS_PER_ENEMY;
	}

	public void BossKilled() {
		_userPoints += POINTS_PER_BOSS;
	}

	public void UpdateUserHP(int newHP) {
		_userHP = newHP;
	}
}
