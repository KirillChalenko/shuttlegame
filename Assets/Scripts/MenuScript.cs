﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Title screen script
/// </summary>
public class MenuScript : MonoBehaviour {

	void OnGUI() {
		const int buttonWidth = 84;
		const int buttonHeight = 60;
		Rect buttonFrame = new Rect (Screen.width / 2 - (buttonWidth / 2), 
		                             (2 * Screen.height / 3) - (buttonHeight / 2), 
		                             buttonWidth, buttonHeight);

		// Draw a button to start the game
		if (GUI.Button(buttonFrame, "Start!")) {
			Application.LoadLevel("Stage1");
		}
	}
}
