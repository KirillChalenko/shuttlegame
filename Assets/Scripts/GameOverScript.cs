﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Start or quit the game
/// </summary>
public class GameOverScript : MonoBehaviour {

	void OnGUI() {
		const int buttonWidth = 120;
		const int buttonHeight = 60;
		Rect buttonFrame = new Rect (Screen.width / 2 - (buttonWidth / 2), 
		                             (Screen.height / 3) - (buttonHeight / 2), 
		                             buttonWidth, buttonHeight);

		if (GUI.Button(buttonFrame, "Retry!")) {
			Application.LoadLevel("Stage1");
		}

		buttonFrame.y = (2 * Screen.height / 3) - (buttonHeight / 2);
		if (GUI.Button(buttonFrame, "Back to menu")) {
			Application.LoadLevel("Menu");
		}
	}
}
