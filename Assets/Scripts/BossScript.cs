﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Boss generic behavior
/// </summary>
public class BossScript : EnemyScript {
	
	private Animator _animator;
	private SpriteRenderer[] _renderers;

	public float minAttackCooldown = 0.5f;
	public float maxAttackCooldown = 2.0f;

	private float _aiCooldown;
	private bool _isAttacking;
	private Vector2 _positionTarget;

	protected override void Awake() {
		base.Awake();
		_animator = GetComponent<Animator>();
		_renderers = GetComponentsInChildren<SpriteRenderer>();
	}
	
	protected override void Start () {	
		base.Start();

		_isAttacking = false;
		_aiCooldown = maxAttackCooldown;
	}

	protected override void Update () {
		if (!_hasSpawn) {
			// spawn the boss if we inside the camera
			// (we check only the first renderer for simplicity)
			if (_renderers[0].IsVisibleFrom(Camera.main)) {
				Spawn();
			}
		}
		else {
			// AI
			//------------------------------------
			// simulate boss random actions (attack, move)
			_aiCooldown -= Time.deltaTime;

			if (_aiCooldown <= 0f) {
				_isAttacking = !_isAttacking;
				_aiCooldown = Random.Range(minAttackCooldown, maxAttackCooldown);
				_positionTarget = Vector2.zero;
				
				// set or unset the attack animation
				_animator.SetBool("Attack", _isAttacking);
			}

			// ------------------------------------
			// attack
			if (_isAttacking) {
				// stop any movement
				_move.direction = Vector2.zero;

				if (_weapon != null && _weapon.enabled && _weapon.CanAttack) {
					_weapon.Attack(true);
					SoundEffectsHelper.Instance.MakeEnemyShotSound();
				}
			}
			else {
				// set random point on the screen
				if (_positionTarget == Vector2.zero) {
					Vector2 randomPoint = new Vector2(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
					_positionTarget = Camera.main.ViewportToWorldPoint(randomPoint);
				}

				// check if we are already there
				if (collider2D.OverlapPoint(_positionTarget)) {
					// reset, will be set at the next frame
					_positionTarget = Vector2.zero;
				}

				// go to the target
				Vector3 direction = (Vector3)_positionTarget - transform.position;
				_move.direction = Vector3.Normalize(direction);
			}


			// destroy the enemy if we outside the camera
			if (!renderer.IsVisibleFrom(Camera.main)) {
				Destroy(gameObject);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D otherCollider2D) {
		// check if we took damage
		ShotScript shot = otherCollider2D.gameObject.GetComponent<ShotScript>();
		if (shot != null && !shot.isEnemyShot) {
			// stop attacking
			_aiCooldown = Random.Range(minAttackCooldown, maxAttackCooldown);
			_isAttacking = false;

			_animator.SetTrigger("Hit");
		}
	}

	void OnDrawGizmos() {
		// display debug information
		if (_hasSpawn && !_isAttacking) {
			Gizmos.DrawSphere(_positionTarget, 0.25f);
		}
	}

	protected override void Spawn() {
		base.Spawn();

		// stop the main scrolling
		foreach (ScrollingScript scrolling in FindObjectsOfType<ScrollingScript>()) {
			if (scrolling.isLinkedToCamera) {
				scrolling.speed = Vector2.zero;
			}
		}
	}
}
