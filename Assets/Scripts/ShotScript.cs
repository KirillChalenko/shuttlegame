﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Projectile behavior
/// </summary>
public class ShotScript : MonoBehaviour {
	
	public int damage = 1;
	// projectile damage player or enemies?
	public bool isEnemyShot = false;

	// Use this for initialization
	void Start () {
		// limited time to live to avoid any leak (10 sec)
		Destroy(gameObject, 10.0f);
	}

	void Update () {
		// destroy the shot if we outside the camera
		if (!renderer.IsVisibleFrom(Camera.main)) {
			Destroy(gameObject);
		}
	}
}
