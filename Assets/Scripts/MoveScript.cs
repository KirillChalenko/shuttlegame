﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simply moves the current game object
/// </summary>
public class MoveScript : MonoBehaviour {

	public Vector2 speed = new Vector2(10.0f, 10.0f);
	public Vector2 direction = new Vector2(-1.0f, 0.0f);
	private Vector2 _movement;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		_movement = new Vector2(speed.x * direction.x, speed.y * direction.y);
	}

	void FixedUpdate()
	{
		// Apply movement to the rigidbody
		rigidbody2D.velocity = _movement;
	}
}
