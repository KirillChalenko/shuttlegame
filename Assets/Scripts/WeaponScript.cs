﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Launch projectile
/// </summary>
public class WeaponScript : MonoBehaviour {

	public Transform shotPrefab;
	// cooldown between two shots (in secs)
	public float shootingRate = 0.25f;
	// current cooldown
	private float _shootCooldown;

	// check is weapon ready to make a shot
	public bool CanAttack {
		get {
			return _shootCooldown <= 0.0f;
		}
	}

	// create a new projectile if possible
	public void Attack(bool isEnemy) {
		if (CanAttack) {
			_shootCooldown = shootingRate;
			
			// create a new shot
			var shotTransform = Instantiate(shotPrefab) as Transform;
			shotTransform.position = transform.position;
			
			ShotScript shot = shotTransform.GetComponent<ShotScript>();
			if (shot != null) {
				shot.isEnemyShot = isEnemy;
			}

			MoveScript move = shotTransform.GetComponent<MoveScript>();
			if (move != null) {
				// Make the weapon shot always towards it
				move.direction = transform.right;
			}
		}
	}

	// Use this for initialization
	void Start () {
		_shootCooldown = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (_shootCooldown > 0) {
			_shootCooldown -= Time.deltaTime;
		}
	}
}
