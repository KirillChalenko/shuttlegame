﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creating instance of sounds from code with no effort
/// </summary>
public class SoundEffectsHelper : MonoBehaviour {

	public static SoundEffectsHelper Instance;
	
	public AudioClip explosionSound;
	public AudioClip playerShotSound;
	public AudioClip enemyShotSound;

	void Awake() {
		if (Instance != null) {
			Debug.LogError("Multiple instances of SoundEffectsHelper!");
		}

		Instance = this;
	}

	public void MakeExplosionSound() {
		MakeSound(explosionSound, 2.0f);
	}
	
	public void MakePlayerShotSound() {
		MakeSound(playerShotSound, 0.3f);
	}
	
	public void MakeEnemyShotSound() {
		MakeSound(enemyShotSound, 0.5f);
	}

	// Play a given sound
	private void MakeSound(AudioClip originalClip, float volume) {
		// As it is not 3D audio clip, position doesn't matter.
		AudioSource.PlayClipAtPoint(originalClip, transform.position, volume);
	}
}
