﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Handle enemies and boss spawning
/// </summary>
public class EnemySpawningScript : MonoBehaviour {

	public Transform enemyPrefab;
	public int maxEnemiesInRow = 5;

	/// <summary>
	/// Enemies spawning positions (X, Y), calculated from camera's top-right corner. 
	/// </summary>
	public Vector2[] spawningPositions;

	// cooldown between two spawnings (in secs)
	public float enemySpawningRate = 10.0f;

	private float _enemySpawningCooldown;
	private List<Vector2> _freeSpawningPositions;

	// Use this for initialization
	void Start () {
		_enemySpawningCooldown = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		// check are we ready to spawn
		if (_enemySpawningCooldown > 0.0f) {
			_enemySpawningCooldown -= Time.deltaTime;
		}
		else {
			SpawnEnemy();
			_enemySpawningCooldown = enemySpawningRate;
		}
	}

	// spawn a new enemy if possible
	void SpawnEnemy() {
		// check if we have any spawning positions
		if (spawningPositions.Length > 0) {
			// get random enemies count
			int enemiesCount = Random.Range(1, maxEnemiesInRow + 1);

			// get the camera position
			float dist = (transform.position - Camera.main.transform.position).z;
			float topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
			float rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;

			// get random enemy positions from the array
			_freeSpawningPositions = new List<Vector2>(spawningPositions);
			for (int i = 0; i < enemiesCount; i++) {
				int enemyPositionIndex = Random.Range(0, _freeSpawningPositions.Count);
				Vector2 enemyPosition = _freeSpawningPositions[enemyPositionIndex];
				_freeSpawningPositions.RemoveAt(enemyPositionIndex);

				// create an enemy object and place it
				var enemyTransform = Instantiate(enemyPrefab) as Transform;
				enemyTransform.position = new Vector3(rightBorder + enemyPosition.x, topBorder + enemyPosition.y);
				enemyTransform.parent = transform;
			}
		}
	}
}
